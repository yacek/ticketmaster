from django.db import models

class Place(models.Model):
	venue_id = models.CharField(max_length=10,unique=True,primary_key=True)
	name = models.CharField(max_length=255)
	address = models.CharField(max_length=255)
	venue_url = models.CharField(max_length=255)
	place_id = models.CharField(max_length=27,blank=True,null=True)
	error_pid = models.NullBooleanField(default=False)


class Event(models.Model):
	event_id = models.CharField(max_length=16,primary_key=True,unique=True)
	name = models.TextField(max_length=255)
	date = models.DateTimeField('Event date')
	minorgenre = models.CharField(max_length=100)
	majorgenre = models.CharField(max_length=100)
	place_id = models.CharField(max_length=27,blank=True,null=True)
	event_info = models.TextField(null=True,blank=True)
	place_key = models.ForeignKey(Place,on_delete=models.CASCADE)









