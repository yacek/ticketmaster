HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Language': 'en,en-US;q=0.7,en;q=0.3',
           'Accept-Encoding': 'gzip, deflate',
           'DNT': '1',
           'Connection': 'keep-alive',
           'Upgrade-Insecure-Requests': '0',
           'Cache-Control': 'max-age=0, no-cache',
           'Pragma': 'no-cache'
           }

REQUEST_URLS = {'music': 'http://www.ticketmaster.com/json/browse/music',
				'sports': 'http://www.ticketmaster.com/json/browse/sports',
				'arts': 'http://www.ticketmaster.com/json/browse/arts',
				'family' : 'http://www.ticketmaster.com/json/browse/family',
				'miscellaneous': 'http://www.ticketmaster.com/json/browse/miscellaneous'
				} 

GMAP_API_KEY = 'AIzaSyCJg5aZYLX6fwGT2h7eXHMYOT70zF7Laio'