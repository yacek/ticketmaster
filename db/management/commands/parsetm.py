from django.core.management.base import BaseCommand
import os
import sys 
import datetime
from dateutil import parser as dparser

from db import tm_us
from tmparse import config


class Command(BaseCommand):
	help = """Parse places and events from tickectmaster.com \n\
			available keys: \n\
			-i, --dma - city ids to parse, 0 - parse all from dma_us.txt \n\
			-s, --date_from  - start date to parse, default - now \n\
			-o, --date_offset - offset of days to parse, default - 1 \n\
			-c, --category - category to parse, [music, family, \
				arts, sports, miscellaneous, all], default - all \n
			-d, --domain - domain of ticket master to be scrapped,\
			default - all"""
	def add_arguments(self,parser):
		parser.add_argument('-i','--dma',dest='dma',type=int,default=[0],nargs='*',
							help='id of city to parse, 0 - all')
		parser.add_argument('-s','--date_from',dest='date_from',type=str,
							default='now',help='Date in YYYY-MM-DD')
		parser.add_argument('-o','--date_offset',dest='date_offset',type=int,
							default=1,help='Day to parse from start date')
		parser.add_argument('-c','--category',dest='category',type=str,nargs='*',
							default=['all'],help='Categoty to parse - [music, family,	\
							arts, sports, miscellaneous, all], default - all')
		parser.add_argument('-d','--domain',dest='domain',type=str,nargs='*',
							default=['all'],help='Domain of tickectmaster to be scrapped')

	def handle(self, *args, **options):	
		category = [x.lower() for x in options['category']]
		domain = [x.lower() for x in options['domain']]
		cat = category_list(category)
		if not cat:
			print('Invalid category')
			return
		dom = domain_list(domain)
		if not dom:
			print('Invalid domain')
			return
		dma = options['dma']	
		try:
			if options['date_from'] == 'now':
				date_from = datetime.datetime.now()
			else:
				date_from = dparser.parse(options['date_from'])
		except ValueError as e:
			print('Ivnalid date %s' % e)
			return
		date_offset = options['date_offset']	
		tm_us.main(dma,date_from,date_offset,cat,dom)

def domain_list(domain):
	dom = []
	domain_list = list(config.REQUEST_DOMAIN.keys())
	if 'all' in domain:
		dom = domain_list
	else:
		for value in domain:
			if value in domain_list:
				dom.append(value)
	return dom

def category_list(category):
	cat = []
	category_list = list(config.REQUEST_URLS.keys())
	if 'all' in category:
		cat = category_list		
	else:
		for value in category:
			if value in category_list:
				cat.append(value)
	return cat