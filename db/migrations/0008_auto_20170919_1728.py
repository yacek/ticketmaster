# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-09-19 14:28
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0007_auto_20170919_1724'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='place_id',
            field=models.CharField(blank=True, max_length=27, null=True),
        ),
        migrations.AlterField(
            model_name='place',
            name='place_id',
            field=models.CharField(blank=True, max_length=27, null=True),
        ),
    ]
