#-*- coding: utf-8 -*-
import time
import datetime
import pytz
import os
import sys
import logging
from dateutil import parser as date_parser
import html.parser as htmlparser
import imp 

import django
import requests 
import googlemaps

import tmparse.config as config
from db import models

logging.basicConfig(handlers=[logging.FileHandler('tmaster.log', 'a', 'utf-8')],
                    level=logging.INFO,
                    format="%(asctime)s:%(levelname)s:%(message)s")

class Place:
	site_url = 'http://www.ticketmaster.com'
	def __init__(self,**kwargs):
		self.venue = kwargs['VenueId']
		self.address = kwargs['address']
		self.place_id = kwargs['place_id']
		self.name = kwargs['VenueName']
		self.url = ''.join([self.site_url,kwargs['VenueSEOLink']])
		self.error_pid = kwargs['error_pid']

	def dump(self):
		self.place = models.Place(venue_id=self.venue,address=self.address,
					  		 place_id=self.place_id,name=self.name,
					  		 venue_url=self.url,error_pid=self.error_pid)
		try:
			self.place.save()
		except db.Error as e:
			logging.error('Place save error %s' % self.venue)

class Event:
	def __init__(self,place,**kwargs):
		self.id = kwargs['Id']
		self.name = kwargs['EventName']
		self.date = kwargs['date']
		self.majorgenre = kwargs['MajorGenre']
		self.minorgenre = kwargs['MinorGenre']
		self.place_id = kwargs['place_id']
		self.place = place
		self.event_info = kwargs['EventInfo']

	def dump(self):
		self.event = models.Event(event_id=self.id,name=self.name,date=self.date,
							 majorgenre=self.majorgenre, minorgenre=self.minorgenre,
							 place_id=self.place_id, place_key=self.place,
							 event_info=self.event_info)
		try:
			self.event.save()
		except db.Error as e:
			logging.error('Event save error %s' % self.id)

def make_request(dma,date_from,date_offset,category,domain):
	"""Make request, retuns josn with place and event data"""
	url = ''.join([config.REQUEST_DOMAIN[domain][0],config.REQUEST_URLS[category]])
	date_to = date_from + datetime.timedelta(days=date_offset)
	payload = {
			   'start': date_from.strftime('%Y-%m-%d'),
			   'end':  date_to.strftime('%Y-%m-%d'),
			   'dma_id': dma
			  }
	try:
		r = requests.get(url,headers=config.HEADERS,params=payload)
		return r.json()['response']['docs']
	except requests.exceptions.RequestException as e:
		logging.error('Request error %s ' % e)
		return None

def get_place_id(address):
	"""Return place id using address"""
	gmap = googlemaps.Client(key=config.GMAP_API_KEY)

	try:
		res = gmap.geocode(address)	
		return res[0]['place_id']
	except IndexError:
		logging.error('Place id not found for %s' % address)
		return None

def get_address(record):
	"""Return address string"""
	fields = [x for x in list(config.VENUE_KEYS[3].values())[0]]
	address = []
	parser = htmlparser.HTMLParser()
	for field in fields:
		if field in record:
			address.append(parser.unescape(record[field]))
	return ', '.join(address)

def get_place(record):
	"""Get place data from json record,return Place object"""
	place = {}
	parser = htmlparser.HTMLParser()
	for field in config.VENUE_KEYS[0:3]:
		if field in record:
			place[field] = parser.unescape(record[field])
		else:
			place[field] = None
	place['address'] = get_address(record)
	check_place = models.Place.objects.filter(venue_id__exact=place['VenueId'])
	if check_place:
		place['place_id'] = list(check_place.values('place_id'))[0]['place_id']
		if place['place_id']:
			place['error_pid'] = False
		else:
			place['error_pid'] = True
		result = Place(**place)
	else:
		placeid = get_place_id(', '.join([place['VenueName'],place['address']]))
		place['place_id'] = placeid
		if placeid:
			place['error_pid'] = False
		else:
			place['error_pid'] = True					
		result = Place(**place)
	return result

def get_event(record,place):
	"""Get event data from json record and Place obj, return event object"""
	event = {}
	parser = htmlparser.HTMLParser()
	ev_keys = config.EVENT_KEYS
	minorg_list = config.MINOR_GENRE
	majorg_list = config.MAJOR_GENRE
	minor_genre_id = int(record[ev_keys['MinorGenreId']][0])
	major_genre_id = int(record[ev_keys['MajorGenreId']][0])
	if minor_genre_id in minorg_list:
		event['MinorGenre'] = minorg_list[minor_genre_id]
	else:
		event['MinorGenre'] = record[ev_keys['MinorGenre']][0]
		logging.error('MinorGenre "%s" not in dict (%s) ' %
					 (minor_genre_id,repr(record[ev_keys['MinorGenre']][0])))
	event['MajorGenre'] = majorg_list[major_genre_id]	
	key_list = [ev_keys[x] for x in ['Id','EventName','EventInfo']]
	for field in key_list:
		if field in record:
			event[field] = parser.unescape(record[field])
		else:
			event[field] = None
	check_event = models.Event.objects.filter(event_id__exact=event['Id'])
	if check_event:
		return None
	event['date'] = date_parser.parse(record[ev_keys['Date'][0]]
											[ev_keys['Date'][1]])
	event['place_id'] = place.place_id
	return Event(place=place.place,**event)
	

def process_data(dma,date_from,date_offset,category,domain):
	"""Process reqeust and save result to database"""
	request = make_request(dma,date_from,date_offset,category,domain)
	if not request:
		return
	for record in request:
		event = {}
		place = get_place(record)
		if place:
			place.dump()
		event = get_event(record,place)		
		if event:
			event.dump()

def get_dma_list(dma,domain):
	"""Return list of city id"""
	file = os.path.join(config.BASE_DIR,config.REQUEST_DOMAIN[domain][1])
	if 0 in dma:
		with open(file,'r') as file:
			dma = file.readlines()
			dma_id = [int(x.split(':')[0]) for x in dma]
	else:
		dma_id = dma
	return dma_id

def main(dma,date_from,date_offset,category,domain):
	"""Main module

	dma - list of integers(city id in ticketmaster), list of available
	dmas in %domain_name%.txt.
	date_from - start date for selected date range
	date_offset - number of days from start date
	category - list of categories to parse, available values :
		music, arts, sports, miscellaneous, family.
	If passed 'all' then parse all available category

	"""
	if date_offset < 0 and date_offset > 20:
		print('Date offset must be positive number')
		return
		
	for dom in domain:
		dma_id = get_dma_list(dma,dom)		
		for id in dma_id:
			for cat in category:
				process_data(id,date_from,date_offset,cat,dom)
				time.sleep(3)
	logging.info('Done')

if __name__ == '__main__':
	print('ticketmaster scpaping module')
	





	


