#-*- coding: utf-8 -*-
import os

HEADERS = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X x.y; rv:42.0) Gecko/20100101 Firefox/42.0',
           'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
           'Accept-Language': 'en,en-US;q=0.7,en;q=0.3',
           'Accept-Encoding': 'gzip, deflate',
           'DNT': '1',
           'Connection': 'keep-alive',
           'Upgrade-Insecure-Requests': '0',
           'Cache-Control': 'max-age=0, no-cache',
           'Pragma': 'no-cache'
           }

REQUEST_URLS = {'music': 'json/browse/music',
				'sports': 'json/browse/sports',
				'arts': 'json/browse/arts',
				'family' : 'json/browse/family',
				'miscellaneous': 'json/browse/miscellaneous'
				}
REQUEST_DOMAIN = {'us': ['http://www.ticketmaster.com/','us.txt'],
                  'ca': ['http://www.ticketmaster.ca/','ca.txt'],
                  'mx': ['http://www.ticketmaster.com.mx/','mx.txt']} 

VENUE_KEYS = ['VenueId', 'VenueName', 'VenueSEOLink',
              {'VenueAddress': ['VenueAddress', 'VenueCityState',
                                'VenueCountry', 'VenuePostalCode']}]
EVENT_KEYS = {'Id': 'Id','EventName': 'EventName','EventInfo': 'EventInfo',
              'MinorGenre': 'MinorGenre', 'MinorGenreId': 'MinorGenreId',
              'MajorGenre': 'MajorGenre', 'MajorGenreId': 'MajorGenreId',
              'Date': ['PostProcessedData','LocalEventDate']}

GMAP_API_KEY = 'AIzaSyCJg5aZYLX6fwGT2h7eXHMYOT70zF7Laio'

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

MINOR_GENRE = { 0: 'More Miscellaneous', 1: 'Rock and Pop', 10: 'Baseball',
                102: 'More Sports', 103: 'Parking', 104: 'Lecture/Seminar', 
                105: 'Expo/Convention', 106: 'Cruise and Sightseeing', 
                107: 'Miscellaneous', 11: 'Soccer', 12: 'Ballet and Dance',
                13: 'Opera', 14: 'Museums and Exhibits', 19: 'Family Attractions',
                2: 'Country and Folk', 200: 'Hard Rock/Metal', 
                201: 'Dance/Electronic', 202: 'R&B/Urban Soul', 203: 'Classical',
                204: 'Volleyball', 205: 'Field Sports', 206: 'Competitions',
                207: 'Musicals', 209: 'Magic Shows', 22: 'Ice Shows', 
                23: "Children's Music and Theater", 238: 'Dinner Package',
                239: 'Concession Vouchers', 25: 'Motorsports', 27: 'Wrestling',
                28: 'Rodeo', 29: 'Circus', 3: 'Rap and Hip-Hop', 30: 'Golf',
                32: 'Plays', 33: 'Boxing', 4: 'Jazz and Blues', 40: 'Latin',
                5: 'World Music', 50: 'New Age and Spiritual', 51: 'Comedy',
                52: 'More Concerts', 53: 'More Arts and Theater', 
                54: 'Fairs and Festivals', 55: 'More Family', 59: 'Movies',
                60: 'Alternative Rock', 61: 'Bull Riding', 645: 'Film Festivals',
                7: 'Basketball', 764: 'Cabaret', 766: 'Festivals' , 8: 'Football',
                830: 'Mixed Martial Arts', 9: 'Hockey', 57 : 'Casino'
              }
MAJOR_GENRE = { 10001: 'Music', 10002: 'Arts & Theater', 10003: 'Family',
                10004: 'Sports', 10005: 'Miscellaneous'
              }
#print(DMA_FILE)
