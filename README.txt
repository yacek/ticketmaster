Ticketmaster parse util.

Usage:
django command parsetm 
			available keys: 
			-i, --dma - city ids to parse, 0 - parse all from dma_us.txt, default = 0
			-s, --date_from  - start date to parse, default - now 
			-o, --date_offset - offset of days to parse, default - 1 
			-c, --category - category to parse, [music, family, arts, sports, miscellaneous, all], default - all
			-d, --domain - domain of ticket master to be scrapped, default - all
City ids available in parsetm dir as txt file.
Domains are in config.py tmparse dir in REQUEST_DOMAIN 

Database has 2 tables:
	Event:
		event_id - unique id of event in ticketmaster
		name - name of event
		date - date of event with timezone
		minorgenre - minorgenre of event, like "Rock", "Football",etc
		majorgenre - major genre of event, available "music, family, arts, sports, miscellaneous"
		place_id - google place id of place for event (may be blank)
		event_info - information of event (may be blank)
	Place:
		venue_id - unique venue id in ticketmaster
		name - name of venue
		address - address of venue
		venue_url - url to venue
		place_id - google place id of place for event (may be blank)
		error_pid - boolean field, True if place_id is blank
